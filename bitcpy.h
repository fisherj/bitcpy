/* -------------------------------------------------------------------- */ /*!

	\file		bitcpy.h

	\brief		Header file for the bitcpy functionality.

	\copyright	(C) 2014 James Fisher (james@waters-fisher.id.au)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/ /* --------------------------------------------------------------------- */

#ifndef __BITCPY_H__
#define __BITCPY_H__

/* -------------------------------------------------------------------- */ /*!

	\details	Copy the required number of bits from the source buffer to the
				destination buffer.

	\author		James Fisher (james@waters-fisher.id.au)

	\date		Tuesday 16 September 2014 08:42:00am 

	\param		_dest		Address of the buffer to write to.
	\param		_write		Bit offset to start writing to.
	\param		_source		Address of the buffer to read from.
	\param		_read		Bit offset to start reading from.
	\param		_count		Number of bits to copy.

	\retval		n/a
	
*/ /* --------------------------------------------------------------------- */

extern void bitcpy(void *_dest, size_t _write,
				   const void *_src, size_t _read,
				   size_t _count);

#endif
