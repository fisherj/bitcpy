#

AR		= ar
CC		= gcc
CPP		= g++
LD		= ld
ECHO	= echo

MAIN	= a

IPATH	= -I.

CFLAGS  =
CFLAGS += -g
CFLAGS += -Wall
CFLAGS += -O0
CFLAGS += $(IPATH)

LFLAGS	= -g
LFLAGS += -lrt
LFLAGS += -Wall
LFLAGS += -lpthread
LFLAGS += -Xlinker -Map -Xlinker $@.map

OBJS	= main.o
OBJS   += bitcpy.o

all: $(MAIN)

$(MAIN): makefile $(OBJS)
	$(CPP) -o $@ $(OBJS)

clean:
	rm -f $(OBJS:.o=.d) $(OBJS:.o=.i) $(OBJS:.o=.s) $(OBJS:.o=.ii)
	rm -f $(OBJS)
	rm -f $(MAIN)
	rm -f $(MAIN).map

-include $(OBJS:.o=.d)

.SUFFIXES: .o .c .h

.c.o:
	$(CC) -MD -c $(CFLAGS) -o $@ $<

love:
	@$(ECHO) "Not war?"

war:
	@$(ECHO) "  /   \\____/   \\I /B/ __ \\B\\A   dCb  88B88B88BC8bBdCb   C88I/B|  ..  |B\\A d8P   8b 88B88   C   88   8b  d8P   8b  88I\\___/|B  |\\___/\\A88B   CC  88  88  88   88  88AC8I   | |_|  |_|B  \\B   88B   88B88  C88  C8P   88   888  88I   | |/|__|\\|B   \\B  88B8P 88B88 d8B8b 88  8b   88B8P  88I   |   |__|A |\\B  C8P  88B88 88B88 88   88   C8P   C88I   |   |__|   |_/  /  \\I   | @ |  | @ || @ |   'I   |   |~~|   ||   |B I   'ooo'  'ooo''ooo'I"  | sed 's/A/        /g' | sed 's/B/    /g' | sed 's/C/8888/g' | sed 's/I/\n/g'

