/* -------------------------------------------------------------------- */ /*!

	\file		main.c

	\brief		Code to test bitcpy 

	\details	

	\copyright	(C) 2014 James Fisher (james@waters-fisher.id.au)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/ /* --------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include "bitcpy.h"

/* -------------------------------------------------------------------- */ /*!

	Test data buffers.

*/ /* --------------------------------------------------------------------- */

unsigned char output[8];
unsigned char input[8];

/* -------------------------------------------------------------------- */ /*!

	\details	Dump the supplied 8 bits of data as binary.

	\author		James Fisher (james@waters-fisher.id.au)

	\date		Wednesday 17 September 2014 09:36:08am 

	\param		_data		Byte to display.

*/ /* --------------------------------------------------------------------- */

void dmp_Binary8bits(unsigned char _data)
{
	int		i;

	for(i = 0; i < 8; ++i)
	{
		printf("%d", (_data & (0x80 >> i)) ? 1 : 0);
	}
}

/* -------------------------------------------------------------------- */ /*!

	\details	Dump the supplied buffer of data as binary.

	\author		James Fisher (james@waters-fisher.id.au)

	\date		Wednesday 17 September 2014 09:36:08am 

	\param		_buffer		Buffer to dump.
	\param		_length		Number of bytes to dump from the buffer.

*/ /* --------------------------------------------------------------------- */

void dmp_BinaryData(unsigned char *_buffer, size_t _length)
{
	int		i;

	for(i = 0; i < _length; ++i)
	{
		dmp_Binary8bits(*_buffer++);
	}
}

/* -------------------------------------------------------------------- */ /*!

	\details

	\author		James Fisher (james@waters-fisher.id.au)

	\date		Wednesday 17 September 2014 09:03:33am 

	\param		_argc
	\param		_argv

	\retval
	
	\see

*/ /* --------------------------------------------------------------------- */

int main(int _argc, char **_argv)
{
	int		i, j, k;

	(void) memset(&input[0], 0xFF, sizeof(input));

	for(i = 1; i <= 32; ++i)
	{
		for(j = 0; j < 16; ++j)
		{
			for(k = 0; k < 16; ++k)
			{
				(void) memset(&output[0], 0x00, sizeof(output));

				printf("%2d:%2d:%2d ", i, k, j);

				bitcpy(&output[0], k, &input[0], j, i);

				dmp_BinaryData(&output[0], 8);

				printf("\n");
			}
		}
	}

	return(0);
}
