/* -------------------------------------------------------------------- */ /*!

	\file		bitcpy.c

	\brief		Code to handle bitcpy 

	\details	Copy bits between source and destination buffers.

	\copyright	(C) 2014 James Fisher (james@waters-fisher.id.au)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/ /* --------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>

#include "bitcpy.h"

/* -------------------------------------------------------------------- */ /*!

	\details	Copy the required number of bits from the source buffer to the
				destination buffer.

	\author		James Fisher (james@waters-fisher.id.au)

	\date		Tuesday 16 September 2014 08:42:00am 

	\param		_dest		Address of the buffer to write to.
	\param		_write		Bit offset to start writing to.
	\param		_source		Address of the buffer to read from.
	\param		_read		Bit offset to start reading from.
	\param		_count		Number of bits to copy.

	\retval		n/a
	
*/ /* --------------------------------------------------------------------- */

void bitcpy(void *_dest, size_t _write,
			const void *_src, size_t _read,
			size_t _count)
{
	unsigned char	data;
	unsigned char	original;
	unsigned char	mask;

	size_t	bitsize;

	size_t	readlhs = _read & 7;
	size_t	readrhs = 8 - readlhs;

	const unsigned char  *source = _src + (_read / 8);

	size_t	writelhs = _write & 7;
	size_t	writerhs = 8 - writelhs;

	unsigned char  *dest = _dest + (_write / 8);

	static const unsigned char	readmask[] = 
	{
		0x00,	/*	== 0	00000000b	*/
		0x80,	/*	== 1	10000000b	*/
		0xC0,	/*	== 2	11000000b	*/
		0xE0,	/*	== 3	11100000b	*/
		0xF0,	/*	== 4	11110000b	*/
		0xF8,	/*	== 5	11111000b	*/
		0xFC,	/*	== 6	11111100b	*/
		0xFE,	/*	== 7	11111110b	*/
		0xFF	/*	== 8	11111111b	*/
	};

	static const unsigned char	writemask[] =
	{
		0xFF,	/*	== 0	11111111b	*/
		0x7F,	/*	== 1	01111111b	*/
		0x3F,	/*	== 2	00111111b	*/
		0x1F,	/*	== 3	00011111b	*/
		0x0F,	/*	== 4	00001111b	*/
		0x07,	/*	== 5	00000111b	*/
		0x03,	/*	== 6	00000011b	*/
		0x01,	/*	== 7	00000001b	*/
		0x00	/*	== 8	00000000b	*/
	};

	while(_count > 0)
	{
		/* Read 8 bits of data from the source buffer.	*/

		data = *source++;

		/* Figure out how many bits we are working with during this			*/
		/* interation.														*/

		bitsize = (_count > 8) ? 8 : _count;

		/* If we are not byte aligned for the source buffer, shift the data	*/
		/* we've already read left by the required amount.					*/

		if(readlhs > 0)
		{
			data = data << readlhs;

			/* If we don't have enough bits left after the shift, read the	*/
			/* extra bits from the top of the next byte inn the buffer.		*/

			if(bitsize > readrhs)
			{
				data = data | (*source >> readrhs);
			}
		}

		/* If we're not using the full 8 bits in the buffer, clear the		*/
		/* right hand side of the bits.										*/

		if(bitsize < 8)
		{
			data = data & readmask[bitsize];
		}

		/* At this point, data holds (up to) 8 bits of data, ready to be	*/
		/* written into the destination buffer.								*/

		original = *dest;

		if(writelhs > 0)
		{
			mask = readmask[writelhs];

			if(bitsize > writerhs)
			{
				*dest++ = (original & mask) | (data >> writelhs);

				original = *dest & writemask[bitsize - writerhs];

				*dest = original | (data << writerhs);
			}
			else
			{
				/* At this point, we are writing bits into the middle of	*/
				/* the original data, so generate a mask to keep the data.	*/

				if((bitsize - writelhs) > 0)
				{
					mask = mask | writemask[8 - (bitsize - writelhs)];
				}

				*dest++ = (original & mask) | (data >> writelhs);
			}
		}
		else
		{
			/* We are byte aligned *and* we have less than 8 bits to write	*/
			/* so read from the destination buffer, mask it and write the	*/
			/* updated data.												*/

			if(bitsize < 8)
			{
				data = data | (original & writemask[bitsize]);
			}

			*dest++ = data;
		}

		_count = _count - bitsize;
	}
}
